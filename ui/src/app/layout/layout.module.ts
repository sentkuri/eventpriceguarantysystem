import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { FooterComponent } from './footer.component';
import { HeaderComponent } from './header.component';
import { RouterModule } from '@angular/router';
import { applayoutRouting } from './layout.routes';
@NgModule({
    imports: [applayoutRouting, CommonModule],
    declarations: [LayoutComponent, FooterComponent, HeaderComponent],
    exports: [LayoutComponent]
    // providers: [AccessGaurd, HeaderService]
})
export class LayoutModule { }
