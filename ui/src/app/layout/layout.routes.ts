import { AuthGuard } from './../guards/auth.guard';
import { LayoutComponent } from './layout.component';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [

    {
        path: '', component: LayoutComponent,

        children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', loadChildren: 'app/home/home.module#HomeModule' , canActivate: [AuthGuard]},
            { path: 'info', loadChildren: 'app/info/info.module#InfoModule' , canActivate: [AuthGuard]},
        ]
    }
];


export const applayoutRouting: ModuleWithProviders = RouterModule.forChild(routes);
