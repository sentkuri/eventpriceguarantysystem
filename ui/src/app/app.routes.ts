import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ModuleWithProviders } from '@angular/core';


const routes: Routes = [
    { path: '', redirectTo: 'app', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'app', loadChildren: 'app/layout/layout.module#LayoutModule' }

];


export const routing: ModuleWithProviders = RouterModule.forRoot(routes);

