import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { LoginModule } from './login/login.module';
import { AppComponent } from './app.component';
import { LayoutModule } from './layout/layout.module';
import { routing } from './app.routes';
import { AuthGuard } from './guards/auth.guard';
import { AuthenticationService } from './common/authentication.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    LoginModule,
    HttpModule,
    LayoutModule,
    routing
  ],
  providers: [AuthenticationService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
