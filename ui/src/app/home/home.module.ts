import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { homeRoutes } from './home.routes';


@NgModule({
    imports: [homeRoutes, CommonModule],
    declarations: [HomeComponent],
    exports: [HomeComponent]
})
export class HomeModule { }
