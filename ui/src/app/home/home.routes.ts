import { HomeComponent } from './home.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const routes = [
    { path: '', component: HomeComponent }
];

export const homeRoutes: ModuleWithProviders = RouterModule.forChild(routes);