import { InfoComponent } from './info.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

const routes = [
    { path: '', component: InfoComponent }
];

export const infoRoutes: ModuleWithProviders = RouterModule.forChild(routes);