import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoComponent } from './info.component';
import { infoRoutes } from './info.routes';


@NgModule({
    imports: [infoRoutes, CommonModule],
    declarations: [InfoComponent],
    exports: [InfoComponent]
})
export class InfoModule { }
