import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class AuthenticationService {
    public token: string;

    public isLoggedIn: Observable<boolean>;
    private _isLoggedIn: BehaviorSubject<boolean>;
    private dataStore: {
        isLoggedIn: boolean
    };

    constructor(private http: Http) {
        // set token if saved in local storage
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
        //Update on AuthenticationService

        this.dataStore = { isLoggedIn: true };
        this._isLoggedIn = <BehaviorSubject<boolean>>new BehaviorSubject(false);
        this.isLoggedIn = this._isLoggedIn.asObservable();
    }

    login(username: string, password: string): Observable<boolean> {

        const body = JSON.stringify({ username: username, password: password });
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this.http.post(environment.serverUrl + '/api/authenticate', body, options)
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let token = response.json() && response.json().token;
                if (token) {
                    // set token property
                    this.token = token;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));
                    this.dataStore = { isLoggedIn: true };
                    this._isLoggedIn.next(Object.assign({}, this.dataStore).isLoggedIn);

                    //this.store.dispatch({ type: LOGIN_USER, payload: { userName: username } });

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
        this.dataStore = { isLoggedIn: false };
        this._isLoggedIn.next(Object.assign({}, this.dataStore).isLoggedIn);
    }


}