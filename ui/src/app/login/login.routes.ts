import { LoginComponent } from './login.component';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: 'ww', component: LoginComponent }
];
export const loginRoutes: ModuleWithProviders = RouterModule.forChild(routes);