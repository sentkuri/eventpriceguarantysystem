import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login.component';
import { CommonModule } from '@angular/common';

import { loginRoutes } from './login.routes';

@NgModule({
    imports: [loginRoutes, FormsModule, CommonModule],
    declarations: [LoginComponent],
    exports: [LoginComponent]
})
export class LoginModule { }
