import { UiBootstrapPage } from './app.po';

describe('ui-bootstrap App', () => {
  let page: UiBootstrapPage;

  beforeEach(() => {
    page = new UiBootstrapPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
